package com.db.grad.d13.context;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.db.grad.d13.core.database.DBConnector;
import com.db.grad.d13.core.database.DBConnectorImpl;
import com.db.grad.d13.core.database.DBStatusService;
import com.db.grad.d13.core.database.DBStatusServiceImpl;
import com.db.grad.d13.core.login.LoginService;
import com.db.grad.d13.core.login.LoginServiceImpl;
import com.db.grad.d13.core.reports.ReportWithMockedData;
import com.db.grad.d13.core.reports.R3Report;
import com.db.grad.d13.core.reports.R3Visualization;
import com.db.grad.d13.core.reports.ReportImpl;
import com.db.grad.d13.core.reports.ReportService;
import com.db.grad.d13.core.reports.ReportServiceImpl;

public class ApplicationContext {
	public final Map<String, Object> context = Collections.unmodifiableMap(createContext());
	
	private Map<String, Object> createContext() {
		Map<String, Object> context = new HashMap<>();
		/* 
		 * Initialize beans there, then put them into context with some name
		 * After addition of beans to the context, they can be autowired(only in servlets) 
		 * with @AutowiredServletDependency(beanName) annotation.
		 * */
		DBConnector dbConnector = new DBConnectorImpl();
		
		LoginService loginService = new LoginServiceImpl(dbConnector);
		context.put("loginService", loginService);
		
		DBStatusService dbStatusService = new DBStatusServiceImpl(dbConnector);
		context.put("dbStatusService", dbStatusService);

		R3Report r3 = new R3Report("Realised Dealer P/L", "mock", dbConnector, 10);
		
		ReportService reportService = new ReportServiceImpl(
			new ReportWithMockedData("Report with mocked data(works without DB connection)", 10),
			
			new ReportImpl("Counterparty Table", "SELECT * FROM db_grad_cs_1917.counterparty", dbConnector, 10),
			new ReportImpl("Deal Table", "SELECT * FROM db_grad_cs_1917.deal", dbConnector, 10),
			new ReportImpl("Instrument Table", "SELECT * FROM db_grad_cs_1917.instrument", dbConnector, 10),
			

            new ReportImpl("Avg. Instrument B/S Prices",
                    "SELECT i.instrument_name, d.deal_type, avg(deal_amount) as average  " +
                    "FROM db_grad_cs_1917.deal d " +
                    "JOIN db_grad_cs_1917.instrument i on i.instrument_id = d.deal_instrument_id " +
                    "GROUP BY d.deal_instrument_id, d.deal_type", dbConnector, 10),
            new ReportImpl("Instrument Usage", "select * from db_grad_cs_1917.instrument " +
				    "INNER JOIN  " +
				    "(select counterparty.counterparty_name, T.deal_instrument_id, T.sum " +
				    "FROM db_grad_cs_1917.counterparty " +
				    "INNER JOIN (select deal_instrument_id, deal_counterparty_id,  " +
				    "SUM(deal_amount * deal_quantity) as sum  from db_grad_cs_1917.deal  " +
				    "group by  db_grad_cs_1917.deal.deal_instrument_id, db_grad_cs_1917.deal.deal_counterparty_id) as T " +
				    "ON db_grad_cs_1917.counterparty.counterparty_id = db_grad_cs_1917.T.deal_counterparty_id) as T1 " +
				    "ON db_grad_cs_1917.instrument.instrument_id = db_grad_cs_1917.T1.deal_instrument_id " +
				    "ORDER BY instrument_name", dbConnector, Integer.MAX_VALUE),
/*
			new ReportImpl("Effective Dealer P/L", EPL_QUERY, dbConnector, 10),
*/			
			new ReportImpl("Deal History", DEAL_HISTORY_QUERY, dbConnector, 10),
/*			
			new ReportImpl("Effective P/L Visualization", 
					"SELECT buy_amount, sell_amount, B.counterparty_name, B.instrument_name, effective_profit_loss FROM " 
					+ "( "
					+ "SELECT " + 
					"	sum(CASE WHEN deal_type = 'B' THEN deal_amount * deal_quantity ELSE 0 END) as buy_amount, " + 
					"	sum(CASE WHEN deal_type = 'S' THEN deal_amount * deal_quantity ELSE 0 END) as sell_amount, " + 
					"	counterparty_name,  " + 
					"	instrument_name " + 
					"	FROM  ( " + DEAL_HISTORY_QUERY +
					"    ) A group by counterparty_name, instrument_name "
					+ ") B "
					+ "JOIN "
					+ "( " + EPL_QUERY + " ) EPLQUERY "
					+ "ON EPLQUERY.counterparty_name = B.counterparty_name "
					+ "AND EPLQUERY.instrument_name = B.instrument_name", dbConnector, Integer.MAX_VALUE),
*/			
			r3,
			
			new R3Visualization("Realized P/L Visualization", 
					"SELECT buy_amount, sell_amount, B.counterparty_name, B.instrument_name FROM " 
					+ "( "
					+ "SELECT " + 
					"	sum(CASE WHEN deal_type = 'B' THEN deal_amount * deal_quantity ELSE 0 END) as buy_amount, " + 
					"	sum(CASE WHEN deal_type = 'S' THEN deal_amount * deal_quantity ELSE 0 END) as sell_amount, " + 
					"	counterparty_name,  " + 
					"	instrument_name " + 
					"	FROM  ( " + DEAL_HISTORY_QUERY +
					"    ) A group by counterparty_name, instrument_name "
					+ ") B ", dbConnector, Integer.MAX_VALUE, r3),

			new ReportImpl("Dealer Ending Positions", "select counterparty_id, counterparty_name, instrument_name, end_pos from db_grad_cs_1917.counterparty"
					+ " JOIN (select deal_counterparty_id, deal_instrument_id, sum(case when deal_type = 'S' then -1 * deal_quantity else deal_quantity end) as end_pos"
					+ " from db_grad_cs_1917.deal group by deal_counterparty_id, deal_instrument_id) as T on counterparty_id = T.deal_counterparty_id "
					+ " JOIN  db_grad_cs_1917.instrument ON instrument_id = deal_instrument_id;", dbConnector, 10),

			

			new ReportImpl("Instrument List", "select instrument_name from db_grad_cs_1917.instrument", dbConnector, Integer.MAX_VALUE),

			new ReportImpl("Counterparty List", "select counterparty_name from db_grad_cs_1917.counterparty", dbConnector, Integer.MAX_VALUE)

			
		);
		
		context.put("reportService", reportService);
		
		/*
		 * Initialized beans will be then added to common servlet context.
		 * At the moment of servlet initialization they will be injected 
		 * (check AutowiredServlet init() method for more).
		 * */		
		return context;
	}
	
	private static final String DEAL_HISTORY_QUERY = "select deal_id, deal_time,counterparty_name, instrument_name, deal_type,   deal_quantity, deal_amount  from db_grad_cs_1917.instrument " +
			"JOIN " +
			"(select  deal_id, deal_time, deal_instrument_id, counterparty_name, deal_amount, deal_quantity, deal_type  FROM db_grad_cs_1917.deal " +
			"JOIN db_grad_cs_1917.counterparty " +
			"ON db_grad_cs_1917.counterparty.counterparty_id = db_grad_cs_1917.deal.deal_counterparty_id) as T " +
			"ON db_grad_cs_1917.instrument.instrument_id = db_grad_cs_1917.T.deal_instrument_id ";
	
	private static final String EPL_QUERY = "" +
            "select c.counterparty_name, i.instrument_name,  " +
            "round((least(  " +
            "(   " +
            "  select sum(deal_quantity)  " +
            "  from db_grad_cs_1917.deal d1  " +
            "  where d1.deal_counterparty_id = dd.deal_counterparty_id  " +
            "    and d1.deal_instrument_id = dd.deal_instrument_id  " +
            "    and d1.deal_type = 'B'  " +
            "),  " +
            "(   " +
            "  select sum(deal_quantity)  " +
            "  from db_grad_cs_1917.deal d2  " +
            "  where d2.deal_counterparty_id = dd.deal_counterparty_id  " +
            "    and d2.deal_instrument_id = dd.deal_instrument_id  " +
            "    and d2.deal_type = 'S'  " +
            ")) * (  " +
            "(  " +
            "  select sum(deal_amount * deal_quantity) / sum(deal_quantity)  " +
            "  from db_grad_cs_1917.deal d3  " +
            "  where d3.deal_counterparty_id = dd.deal_counterparty_id  " +
            "    and d3.deal_instrument_id = dd.deal_instrument_id  " +
            "    and d3.deal_type = 'S'  " +
            ") - (  " +
            "  select sum(deal_amount * deal_quantity) / sum(deal_quantity)  " +
            "  from db_grad_cs_1917.deal d4  " +
            "  where d4.deal_counterparty_id = dd.deal_counterparty_id  " +
            "    and d4.deal_instrument_id = dd.deal_instrument_id  " +
            "    and d4.deal_type = 'B'  " +
            ")) +  " +
            "case  " +
            "  when (   " +
            "    select sum(deal_quantity)  " +
            "    from db_grad_cs_1917.deal d1  " +
            "    where d1.deal_counterparty_id = dd.deal_counterparty_id  " +
            "      and d1.deal_instrument_id = dd.deal_instrument_id  " +
            "      and d1.deal_type = 'B'  " +
            "    ) - (  " +
            "    select sum(deal_quantity)  " +
            "    from db_grad_cs_1917.deal d1  " +
            "    where d1.deal_counterparty_id = dd.deal_counterparty_id  " +
            "      and d1.deal_instrument_id = dd.deal_instrument_id  " +
            "      and d1.deal_type = 'S' ) > 0  " +
            "  then ((   " +
            "    select sum(deal_quantity)  " +
            "    from db_grad_cs_1917.deal d1  " +
            "    where d1.deal_counterparty_id = dd.deal_counterparty_id  " +
            "      and d1.deal_instrument_id = dd.deal_instrument_id  " +
            "      and d1.deal_type = 'B'  " +
            "    ) - (  " +
            "    select sum(deal_quantity)  " +
            "    from db_grad_cs_1917.deal d1  " +
            "    where d1.deal_counterparty_id = dd.deal_counterparty_id  " +
            "      and d1.deal_instrument_id = dd.deal_instrument_id  " +
            "      and d1.deal_type = 'S' )) * (  " +
            "    select avg(deal_amount)  " +
            "    from db_grad_cs_1917.deal  " +
            "    where deal_time = (  " +
            "      select max(deal_time)  " +
            "      from db_grad_cs_1917.deal  " +
            "      where deal_instrument_id = dd.deal_instrument_id  " +
            "        and deal_type = 'S'  " +
            "    ) and deal_instrument_id = dd.deal_instrument_id  " +
            "      and deal_type = 'S'  " +
            "    )  " +
            "  else ((   " +
            "    select sum(deal_quantity)  " +
            "    from db_grad_cs_1917.deal d1  " +
            "    where d1.deal_counterparty_id = dd.deal_counterparty_id  " +
            "      and d1.deal_instrument_id = dd.deal_instrument_id  " +
            "      and d1.deal_type = 'B'  " +
            "    ) - (  " +
            "    select sum(deal_quantity)  " +
            "    from db_grad_cs_1917.deal d1  " +
            "    where d1.deal_counterparty_id = dd.deal_counterparty_id  " +
            "      and d1.deal_instrument_id = dd.deal_instrument_id  " +
            "      and d1.deal_type = 'S' )) * (  " +
            "    select avg(deal_amount)  " +
            "    from db_grad_cs_1917.deal  " +
            "    where deal_time = (  " +
            "      select max(deal_time)  " +
            "      from db_grad_cs_1917.deal  " +
            "      where deal_instrument_id = dd.deal_instrument_id  " +
            "        and deal_type = 'B'  " +
            "    ) and deal_instrument_id = dd.deal_instrument_id  " +
            "      and deal_type = 'B'  " +
            "    )  " +
            "end), 2) as effective_profit_loss  " +
            "from db_grad_cs_1917.deal dd  " +
            "  join db_grad_cs_1917.counterparty c on c.counterparty_id = dd.deal_counterparty_id  " +
            "    join db_grad_cs_1917.instrument i on i.instrument_id = dd.deal_instrument_id  " +
            "group by deal_counterparty_id, deal_instrument_id";
}