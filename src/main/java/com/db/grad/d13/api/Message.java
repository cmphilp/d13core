package com.db.grad.d13.api;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = MessageSerializer.class)
public class Message { 
	private final String messageType;
	private final Map<String, Object> fields;
	public Message(String type) {
		this.messageType = type;
		fields = new HashMap<>();
	}
	
	public Message setField(String name, Object value){
		fields.put(name, value);
		return this;
	}

	public String getMessageType() {
		return messageType;
	}

	public Map<String, Object> getFields() {
		return fields;
	}

	public Object getField(String name) {
		return fields.get(name);
	}
}