package com.db.grad.d13.api;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class MessageSerializer extends StdSerializer<Message> {
    
    public MessageSerializer() {
        this(null);
    }
   
    public MessageSerializer(Class<Message> t) {
        super(t);
    }
 
    @Override
    public void serialize(Message value, JsonGenerator jgen, SerializerProvider provider) 
      throws IOException, JsonProcessingException {
  
        jgen.writeStartObject();
        jgen.writeStringField("messageType", value.getMessageType());
        for (String field : value.getFields().keySet()) {
        	jgen.writeObjectField(field, value.getFields().get(field));
        }
        jgen.writeEndObject();
    }
}