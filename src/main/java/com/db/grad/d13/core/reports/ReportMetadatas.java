package com.db.grad.d13.core.reports;

public class ReportMetadatas {
	public final static ReportMetadatas REPORT_METADATA = new ReportMetadatas(
		new ReportMetadata("Deal History", "Deal id", "table", true,
				new Column("Deal id", "deal_id", "number"),
				new Column("Counterparty", "counterparty_name", "string"),
				new Column("Instrument", "instrument_name", "string"),
				new Column("Deal type", "deal_type", "string"),
				new Column("Deal amount", "deal_amount", "number"),
				new Column("Timestamp", "deal_time", "string"),
				new Column("Quantity", "deal_quantity", "number")
		),
		
		new ReportMetadata("Instrument Usage", "instrument_name", "glare", true,
				new Column("instrument_name", "string"),
				new Column("counterparty_name", "string"),
				new Column("sum", "number")
		),
/*
        new ReportMetadata("Effective Dealer P/L", "Counterparty name", "table", true,
                new Column("Counterparty name", "counterparty_name", "string"),
                new Column("Instrument name", "instrument_name", "string"),
                new Column("Effective profit / loss", "effective_profit_loss", "number")
        ),
*/
/*
        new ReportMetadata("Effective P/L Visualization", "Counterparty", "EPLV", true,
        		new Column("Buy amount", "buy_amount", "number"),
        		new Column("Sell amount", "sell_amount", "number"),
        		new Column("Effective profit/loss", "effective_profit_loss", "number"),
        		new Column("Counterparty", "counterparty_name", "string"),
        		new Column("Instrument", "instrument_name", "string")
        ),
*/
		new ReportMetadata("Realized P/L Visualization", "Counterparty", "EPLV", true,
        		new Column("Buy amount", "buy_amount", "number"),
        		new Column("Sell amount", "sell_amount", "number"),
        		new Column("Effective profit/loss", "net_sum", "number"),
        		new Column("Counterparty", "counterparty_name", "string"),
        		new Column("Instrument", "instrument_name", "string")
        ),
		
		new ReportMetadata("Avg. Instrument B/S Prices", "Instrument", "table", true,
                new Column("Instrument", "instrument_name", "string"),
                new Column("Deal type", "deal_type", "string"),
                new Column("Average", "average", "number")
        ),
		
        new ReportMetadata("Dealer Ending Positions", "Counterparty id", "table", true,
        		new Column("Counterparty id", "counterparty_id", "number"),
        		new Column("Counterparty name", "counterparty_name", "string"),
        		new Column("Instrument name", "instrument_name", "string"),
        		new Column("Ending position", "end_pos", "number")
        ),
        
        new ReportMetadata("Realised Dealer P/L", "Counterparty id", "table", true,
        		new Column("Counterparty id", "counterparty_id", "number"),
        		new Column("Counterparty name", "counterparty_name", "string"),
        		new Column("Instrument name", "instrument_name", "string"),
        		new Column("Net sum", "net_sum", "number")
        ),
        
        new ReportMetadata("Counterparty List", "counterparty_name", "table", false,
        		new Column("counterparty_name", "string")
        ),
        
        new ReportMetadata("Instrument List", "instrument_name", "table", false,
        		new Column("instrument_name", "string")
        ),
        
		new ReportMetadata("Counterparty Table", "counterparty_id", "table", true,
				new Column("counterparty_id", "number"),
				new Column("counterparty_name", "string"),
				new Column("counterparty_status", "string"),
				new Column("counterparty_date_registered", "string")
		),
		
		new ReportMetadata("Deal Table", "deal_id", "table", true,
				new Column("deal_id", "number"),
				new Column("deal_counterparty_id", "number"),
				new Column("deal_instrument_id", "number"),
				new Column("deal_type", "string"),
				new Column("deal_amount", "number"),
				new Column("deal_time", "string"),
				new Column("deal_quantity", "number")
		),

		new ReportMetadata("Instrument Table", "instrument_id", "table", true,
				new Column("instrument_id", "number"),
				new Column("instrument_name", "string")
		),
        
		/*
		 * Mocked report which works without db connection
		 * */
		new ReportMetadata("Report with mocked data(works without DB connection)", "col1", "table", false,
				new Column("col1", "number"),
				new Column("col2", "string")
		)
		/*
		 * */
	);
	
	public final ReportMetadata[] reportMetadatas;
	
	public ReportMetadatas(ReportMetadata... reportMetadatas) {
		this.reportMetadatas = reportMetadatas;
	}

	public ReportMetadata reportMetadataByName(String name) {
		for (ReportMetadata reportMetadata : reportMetadatas) {
			if (reportMetadata.name.equals(name))
				return reportMetadata;
		}
		return null;
	}
	
	public static class ReportMetadata {
		public final String name;
		public final String defaultSortColumn;
		public final String reportType;
		public final Column[] columns;
		public final boolean showInSelector;
		
		public ReportMetadata(String name, String defaultSortColumn, String reportType, boolean showInSelector, Column... columns) {
			this.name = name;
			this.defaultSortColumn = defaultSortColumn;
			this.reportType = reportType;
			this.showInSelector = showInSelector;
			this.columns = columns;
		}	

		public Column columnByDbName(String dbName) {
			for (Column column : columns) {
				if (column.dbName.equals(dbName))
					return column;
			}
			return null;
		}
		
		public Column columnByName(String name) {
			for (Column column : columns) {
				if (column.name.equals(name))
					return column;
			}
			return null;
		}
	}
	
	public static class Column {
		public final String name;
		public final transient String dbName;
		public final String type;
		
		public Column(String name, String type) {
			this.name = name;
			this.dbName = name;
			this.type = type;
		}
		
		public Column(String name, String dbName, String type) {
			this.name = name;
			this.dbName = dbName;
			this.type = type;
		}
	}
}