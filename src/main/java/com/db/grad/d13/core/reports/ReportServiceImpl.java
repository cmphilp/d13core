package com.db.grad.d13.core.reports;

import java.util.HashMap;
import java.util.Map;

import com.db.grad.d13.api.Message;

public class ReportServiceImpl implements ReportService {
	private final Map<String, Report> reports;
	
	public ReportServiceImpl(Report ... reports) {
		this.reports = new HashMap<String, Report>();
		for (Report report : reports) {
			this.reports.put(report.getReportName(), report);
		}
	}
	
	public Message viewReport(Map<String, String> parameters){
		String reportName = parameters.get("reportName");
		System.out.println("Looking up report " + reportName);
		Report report = reports.get(reportName);
		if (report != null) {
			System.out.println("Viewing report " + reportName + 
					" using view parameters: " + parameters);
		} else {
			System.out.println("Report " + reportName + " is not found");
		}
		Message metadata = new Message("queryMetadata");
		return new Message("report").setField("data", report.get(parameters, metadata))
									.setField("hasMore", metadata.getField("hasMore"))
									.setField("requestedPage", parameters.get("page"));
	}
}
