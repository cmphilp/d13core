package com.db.grad.d13.core.reports;

import java.io.IOException;

import com.db.grad.d13.core.reports.ReportMetadatas.ReportMetadata;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class ReportMetadatasSerializer extends StdSerializer<ReportMetadatas> {
    
    public ReportMetadatasSerializer() {
        this(null);
    }
   
    public ReportMetadatasSerializer(Class<ReportMetadatas> t) {
        super(t);
    }
 
    @Override
    public void serialize(ReportMetadatas value, JsonGenerator jgen, SerializerProvider provider) 
      throws IOException, JsonProcessingException {
  
        jgen.writeStartObject();
        jgen.writeArrayFieldStart("reports");
        for (ReportMetadata reportMetadata : value.reportMetadatas) {
        	jgen.writeObjectField("report", reportMetadata);
        	
        }
        jgen.writeEndArray();
        jgen.writeEndObject();
    }
}