package com.db.grad.d13.core.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.db.grad.d13.api.Message;

public class DBStatusServiceImpl implements DBStatusService{
	private DBConnector dbConnector;
	
	public DBStatusServiceImpl(DBConnector dbConnector) {
		this.dbConnector = dbConnector;
	}

	@Override
	public Message checkDBConnection() {
		Message message = new Message("dbConnection").setField("result", NOTCONNECTED);
		try {
			if (dbConnector.isConnectedToDatabase()) {
				message = new Message("dbConnection").setField("result", CONNECTED);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return message;
	}
	
	

}
