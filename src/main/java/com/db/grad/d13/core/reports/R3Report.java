package com.db.grad.d13.core.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.db.grad.d13.core.database.DBConnector;
import com.db.grad.d13.core.reports.ReportMetadatas.Column;

public class R3Report extends ReportImpl {	
	public R3Report(String reportName, String sqlQuery, DBConnector dbConnector, int pageSize) {
		super(reportName, sqlQuery, dbConnector, pageSize);
	}	
	
	@Override
	protected List<Map<String, Object>> getDbData() throws SQLException {
		
		System.out.println("start");/*
		List<Map<String, Object>> a=  dbConnector.executeQuery(""
				+ "select least(  "
				+ "(        select sum(deal_quantity)         "
				+ "from db_grad_cs_1917.deal d1            "
				+ "where d1.deal_counterparty_id = 701                 "
				+ "and d1.deal_instrument_id = 1001              "
				+ "and d1.deal_type = 'B'  ),    "
				
				+ "(         select sum(deal_quantity)       "
				+ "from db_grad_cs_1917.deal d2  " + 
				"  where d2.deal_counterparty_id = 701               "
				+ "and d2.deal_instrument_id = 1001  " + 
				"  and d2.deal_type = 'S'  ))"
				
				+ " * (  "
				
				+ "(       "
				+ "select sum(deal_amount * deal_quantity) / sum(deal_quantity)            "
				+ "from db_grad_cs_1917.deal d3          "
				+ "where d3.deal_counterparty_id = 701               "
				+ "and d3.deal_instrument_id = 1001                 "
				+ " and d3.deal_type = 'S'  ) "
				
				+ "- "
				
				+ "(   "
				+ "select sum(deal_amount * deal_quantity) / sum(deal_quantity)      "
				+ "from db_grad_cs_1917.deal d4          "
				+ "where d4.deal_counterparty_id = 701  " + 
				"and d4.deal_instrument_id = 1001                  "
				+ "and d4.deal_type = 'B'  )) as net_sum  "
				+ "from db_grad_cs_1917.deal dd  ");
		System.out.println("end");
		System.out.println(a);
		return a;
		*/
		
		return dbConnector.executeQuery("SELECT counterparty_id, counterparty_name FROM db_grad_cs_1917.counterparty")
					.stream()
					.flatMap(							
							z -> {							
								try {
									return dbConnector.executeQuery("SELECT instrument_id, instrument_name FROM db_grad_cs_1917.instrument")
									.stream()
									.flatMap((Map<String, Object> x)->{
										try {											
											String currentCounterparty = z.get("counterparty_id").toString();
											String currentInstrument = x.get("instrument_id").toString();
											return dbConnector
													.executeQuery(R3QUERY.replaceAll("\\?i", currentInstrument)
																		.replaceAll("\\?c", currentCounterparty))
													.stream()
													.map(y->{
														y.put("counterparty_name", z.get("counterparty_name"));
														y.put("counterparty_id", z.get("counterparty_id"));
														y.put("instrument_name", x.get("instrument_name"));
														y.put("instrument_id", x.get("instrument_id"));
														return y;
													});
										} catch (SQLException e) {
											e.printStackTrace();
											return null;
										}
									});		
	
								} catch (SQLException e) {
									e.printStackTrace();
									return null;
								}							
							})
					.collect(Collectors.toList());
		
		
				
	}
	
	
}
