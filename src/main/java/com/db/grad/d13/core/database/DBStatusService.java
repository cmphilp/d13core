package com.db.grad.d13.core.database;

import com.db.grad.d13.api.Message;

public interface DBStatusService {
	public final static String CONNECTED = "Database Connected";
	public final static String NOTCONNECTED = "Database Not Connected";
	
	Message checkDBConnection();
}
