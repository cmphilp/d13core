package com.db.grad.d13.core.reports;

import java.util.Map;

import com.db.grad.d13.api.Message;

public interface ReportService {
	Message viewReport(Map<String, String> parameters);
}
