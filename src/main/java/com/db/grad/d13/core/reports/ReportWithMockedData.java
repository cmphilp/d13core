package com.db.grad.d13.core.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.db.grad.d13.core.database.DBConnector;

public class ReportWithMockedData extends ReportBase {	
	public ReportWithMockedData(String reportName, int linesPerPage){
		super(reportName, linesPerPage, null, null, null);
	}	

	@Override
	protected String getType(String columnName){
		return dataDescriptor.get(columnName);
	}

	@Override
	protected List<Map<String, Object>> getData(){
		List<Map<String, Object>> list = new ArrayList<>();
		list.add(dataRow(1, "Lorem"));
		list.add(dataRow(3, "ipsum"));
		list.add(dataRow(5, "dolor"));
		list.add(dataRow(6, "sit"));
		list.add(dataRow(2, "ololo"));
		list.add(dataRow(7, "oooo"));
		return list;
	}
	
	Map<String, String> dataDescriptor = new HashMap<String, String>(){{
		put("col1", "number");
		put("col2", "string");
	}};
	
	private Map<String, Object> dataRow(Object data1, Object data2){
		Map<String, Object> row = new HashMap<String, Object>(){{
			put("col1", data1);
			put("col2", data2);
		}};
		return row;
	}
}
