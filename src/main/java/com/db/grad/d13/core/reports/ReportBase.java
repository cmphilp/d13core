package com.db.grad.d13.core.reports;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.db.grad.d13.api.Message;
import com.db.grad.d13.core.database.DBConnector;
import com.db.grad.d13.core.reports.ReportMetadatas.ReportMetadata;

public abstract class ReportBase implements Report {
	 R3Report r3;
	protected final String reportName;
	protected final List<Map<String, Object>> data;
	protected final int linesPerPage;
	protected final String sqlQuery;
	protected final DBConnector dbConnector;	
	protected final ReportMetadata reportMetadata;
	
	protected ReportBase(String reportName, int linesPerPage, String sqlQuery, DBConnector dbConnector,  R3Report r3) {
		this.reportName = reportName;
		this.linesPerPage = linesPerPage;
		this.sqlQuery = sqlQuery;
		this.dbConnector = dbConnector;
		reportMetadata = ReportMetadatas.REPORT_METADATA.reportMetadataByName(reportName);
		this.r3 = r3;
		data = getData();
	}
	
	@Override
	public List<Map<String, Object>> get(Map<String, String> parameters, Message requestMetadata) {
		System.out.println("Generating report " + reportName + " for parameters " + parameters);
		List<Map<String, Object>> resultBeforeLimit =  data.stream()
				.map(x -> {
					Map<String, Object> newRow = new HashMap<>();
					for (Map.Entry<String, Object> e : x.entrySet()){
						String key = e.getKey();
						Object value = e.getValue();
						if (getType(key).equalsIgnoreCase("string")) {
							newRow.put(key, value.toString());
						} else if(getType(key).equalsIgnoreCase("number")) {
							newRow.put(key, value);
						} else throw new IllegalArgumentException("illegal datatype: " + getType(key));					
					}
					return newRow;
				})
				.filter(filterWithParameters(parameters))
				.sorted((x, y) -> {
					String sortedColumnName = parameters.get("sortColumn");
					int sortDirection = "ASC".equalsIgnoreCase(parameters.get("sortDirection")) || 
										"Ascending".equalsIgnoreCase(parameters.get("sortDirection")) ? 1 : -1;
					return sortDirection * getRowDataElementAsComparable(x, sortedColumnName)
							.compareTo(getRowDataElementAsComparable(y, sortedColumnName));
				})
				.skip(Integer.parseInt(parameters.get("page")) * linesPerPage)
				.collect(Collectors.toList());
		if (requestMetadata != null) {
			requestMetadata.setField("hasMore", ""+(resultBeforeLimit.size() > linesPerPage));
		}
		return resultBeforeLimit.stream().limit(linesPerPage).collect(Collectors.toList());
	}

	@Override
	public List<Map<String, Object>> get(Map<String, String> parameters) {
		return get(parameters, null);
	}
	
	@Override
	public String getReportName() {
		return reportName;
	}
	
	protected String getType(String columnName){
		return ReportMetadatas.REPORT_METADATA.reportMetadataByName(reportName).columnByName(columnName).type;
	}

	protected abstract List<Map<String, Object>> getData();
	
	private Comparable getRowDataElementAsComparable(Map<String, Object> row, String sortedColumnName) {
		return (Comparable)row.get(sortedColumnName);
	}
	
	private Predicate<Map<String, Object>> filterWithParameters(Map<String, String> parameters){
		return x -> {
			for (Map.Entry<String, Object> e : x.entrySet()){
				String columnName = e.getKey();
				if (parameters.containsKey("filter_" + columnName)) {
					String filter = parameters.get("filter_" + columnName);
					if (getType(columnName).equalsIgnoreCase("string")) {
						if (!containsIgnoreCase(e.getValue().toString(), filter)) {
							return false;
						}
					} else {
						int separatorIndex = filter.indexOf(";");
						double from = Double.parseDouble(filter.substring(0, separatorIndex));
						double to = Double.parseDouble(filter.substring(separatorIndex + 1));
						double value = ((Number) e.getValue()).doubleValue();
						if (!(from <= value && value <= to)) {
							return false;
						}
					}
				}						
			}
			return true;
		};
	}
	
	private boolean containsIgnoreCase(String text, String pattern){
		return text.toLowerCase().contains(pattern.toLowerCase());
	}	
	

	protected final String R3QUERY = "select least(  " + 
			"(   " + 
			"	  " + 
			"select sum(deal_quantity)  " + 
			"	  " + 
			"from db_grad_cs_1917.deal d1  " + 
			"	  " + 
			"where d1.deal_counterparty_id = ?c  " + 
			"		  " + 
			"and d1.deal_instrument_id = ?i  " + 
			"		  " + 
			"and d1.deal_type = 'B'  " + 
			"),  " + 
			"(   " + 
			"	  " + 
			"select sum(deal_quantity)  " + 
			"	  " + 
			"from db_grad_cs_1917.deal d2  " + 
			"	  " + 
			"where d2.deal_counterparty_id = ?c  " + 
			"		  " + 
			"and d2.deal_instrument_id = ?i  " + 
			"		  " + 
			"and d2.deal_type = 'S'  " + 
			")) * (  " + 
			"(  " + 
			"	  " + 
			"select sum(deal_amount * deal_quantity) / sum(deal_quantity)  " + 
			"	  " + 
			"from db_grad_cs_1917.deal d3  " + 
			"	  " + 
			"where d3.deal_counterparty_id = ?c  " + 
			"		  " + 
			"and d3.deal_instrument_id = ?i  " + 
			"		  " + 
			"and d3.deal_type = 'S'  " + 
			") - (  " + 
			"	  " + 
			"select sum(deal_amount * deal_quantity) / sum(deal_quantity)  " + 
			"	  " + 
			"from db_grad_cs_1917.deal d4  " + 
			"	  " + 
			"where d4.deal_counterparty_id = ?c  " + 
			"		  " + 
			"and d4.deal_instrument_id = ?i  " + 
			"		  " + 
			"and d4.deal_type = 'B'  " + 
			")) as net_sum";
}
