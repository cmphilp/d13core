package com.db.grad.d13.core.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBConnectorImpl implements DBConnector {
	
	final private String url = "jdbc:mysql://10.11.32.21:3306/db_grad_cs_1917";
	final private String user = "dbgrad";
	final private String password = "dbgrad";


    public List<Map<String, Object>> executeQuery(String query) throws SQLException {

        List<Map<String, Object>> result = new ArrayList<>();
        try {
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        try (
	                Connection con = DriverManager.getConnection(url, user, password);
	                Statement st = con.createStatement();
	                ResultSet resultSet = st.executeQuery(query)) {
	
	
	            ResultSetMetaData meta = resultSet.getMetaData();
	            int columnCount = meta.getColumnCount();
	
	            while (resultSet.next()) {
	            	Map<String, Object> map = new HashMap<>();
	                for (int i = 1; i <= columnCount; i++) {	                    
	                    map.put(
	                            meta.getColumnName(i),
	                            resultSet.getObject(i)
	                    );
	                }
	                result.add(map);
	            }
	            return result;
	        }
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
        	return Collections.EMPTY_LIST;
        }
    }
    
    @Override
    public boolean isConnectedToDatabase() throws SQLException {
        try {
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        try (Connection con = DriverManager.getConnection(url, user, password);) {
	        	
	        } catch (SQLException e) {
	        	e.printStackTrace();
	        	return false;
	        }
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
        	return false;
        }
        return true;
    }
}