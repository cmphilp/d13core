package com.db.grad.d13.core.login;

import com.db.grad.d13.api.Message;

public interface LoginService { 
	public final static String SUCCESS = "success";
	public final static String FAILURE = "failure";
	
	Message checkPermission(String login, String password);
}