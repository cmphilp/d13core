package com.db.grad.d13.core.login;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.db.grad.d13.api.Message;
import com.db.grad.d13.core.database.DBConnector;
import com.db.grad.d13.core.util.HashUtil;

public class LoginServiceImpl implements LoginService{ 
	private final DBConnector dbConnector;		
	private final List<Map<String, Object>> credentialsList;
	
	public LoginServiceImpl(DBConnector dbConnector) {
		this.dbConnector = dbConnector;
		List<Map<String, Object>> credentialsList;
		try {
			credentialsList = dbConnector.executeQuery("select * from db_grad_cs_1917.users");
		} catch (SQLException e) {
			credentialsList = new ArrayList<Map<String, Object>>();
		}
		
		/*
		 * Mocked credentials that will work even without db connection
		 * */
		//credentialsList.add(new HashMap<String, Object>(){{
		//	put("user_id", "admin");
		//	put("user_pwd", "password");
		//}});
		/*
		 * */
				
		this.credentialsList = Collections.unmodifiableList(credentialsList);		
	}
	
	public Message checkPermission(String login, String password) {
		for (Map<String, Object> entry : credentialsList) {
			String loginFromEntry = (String) entry.get("user_id");
			String passwordFromEntry = (String) entry.get("user_pwd");
			if (loginFromEntry.equals(login) && 
					HashUtil.hashPasswordToHex(passwordFromEntry).equals(password)) {
				return new Message("login").setField("result", SUCCESS);
			}
		}

		return new Message("login").setField("result", FAILURE);
	}
}