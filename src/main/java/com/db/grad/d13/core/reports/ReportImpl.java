package com.db.grad.d13.core.reports;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.db.grad.d13.core.database.DBConnector;
import com.db.grad.d13.core.reports.ReportMetadatas.Column;

public class ReportImpl extends ReportBase {	
	public ReportImpl(String reportName, String sqlQuery, DBConnector dbConnector, int pageSize, R3Report r3) {
		super(reportName, pageSize, sqlQuery, dbConnector, r3);
	}	

	public ReportImpl(String reportName, String sqlQuery, DBConnector dbConnector, int pageSize) {
		super(reportName, pageSize, sqlQuery, dbConnector, null);
	}	
	@Override
	protected List<Map<String, Object>> getData() {
		System.out.println("Fetching data for " + reportName + " report");
		for (Column columnMetadata : reportMetadata.columns) {
			System.out.println("column db name: " + columnMetadata.dbName + " column name " + columnMetadata.name);
		}
		List<Map<String, Object>> list;
		try {
			List<Map<String, Object>> rawData = getDbData();
			if (rawData.size() > 0) {
				System.out.println("Raw data sample: " + rawData.get(0));
			}
			list = rawData.stream()
					.map((Map<String, Object> row)->{
						Map<String, Object> newRow = new HashMap<String, Object>();
						for (String dbColumnName: row.keySet()) {
							if (reportMetadata.columnByDbName(dbColumnName) != null) {
								newRow.put(
									reportMetadata.columnByDbName(dbColumnName).name,
									row.get(dbColumnName)
								);
							}
						}
						return newRow;
					})
					.collect(Collectors.toList());
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.EMPTY_LIST;
		}
	}
	
	protected List<Map<String, Object>> getDbData() throws SQLException {
		return dbConnector.executeQuery(sqlQuery);
	}
	
}
