package com.db.grad.d13.core.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.db.grad.d13.core.database.DBConnector;
import com.db.grad.d13.core.reports.ReportMetadatas.Column;

public class R3Visualization extends ReportImpl {	
	public R3Visualization(String reportName, String sqlQuery, DBConnector dbConnector, int pageSize, R3Report r3) {
		super(reportName, sqlQuery, dbConnector, pageSize, r3);
	}	
	
	@Override
	protected List<Map<String, Object>> getDbData() throws SQLException {
		List<Map<String, Object>> query = dbConnector.executeQuery(sqlQuery);
		for (Map<String, Object> row : query) {
			for (Map<String, Object> r3row : r3.data) {
				if (r3row.get("Counterparty name").equals(row.get("counterparty_name")) &&
						r3row.get("Instrument name").equals(row.get("instrument_name"))) {
					row.put("net_sum", r3row.get("Net sum"));
					System.out.println("====>" + row);
					break;
				}
			}
		}
		return query;
	}
	
	
}
