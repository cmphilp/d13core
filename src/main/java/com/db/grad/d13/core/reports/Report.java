package com.db.grad.d13.core.reports;

import java.util.List;
import java.util.Map;

import com.db.grad.d13.api.Message;

public interface Report {
    public List<Map<String, Object>> get(Map<String, String> parameters);
    public List<Map<String, Object>> get(Map<String, String> parameters, Message message);
	public String getReportName();
}
