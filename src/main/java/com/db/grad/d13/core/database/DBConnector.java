package com.db.grad.d13.core.database;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DBConnector {
    List<Map<String, Object>> executeQuery(String query) throws SQLException;
    boolean isConnectedToDatabase() throws SQLException;
}