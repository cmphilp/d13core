package com.db.grad.d13.core.database;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.*;
import java.util.List;
import java.util.Map;

import com.db.grad.d13.core.database.DBConnectorImpl; 

public class DBConnectorTest{
	
	@Ignore("need to have db running")
	@Test

	public void testDBConnector() {
		
		DBConnectorImpl testConnector = new DBConnectorImpl();
		try {
			List<Map<String, Object>> list = testConnector.executeQuery("SELECT * from db_grad.counterparty");
			assertEquals(20, list.size());
			System.out.printf("Size of list of queried elements {}",list.size());
			assertEquals("[Lina]", list.get(1).values().toString());
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
	}

}
