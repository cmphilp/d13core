package com.db.grad.d13.core.reports;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.db.grad.d13.core.reports.ReportMetadatas.Column;
import com.db.grad.d13.core.reports.ReportMetadatas.ReportMetadata;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ReportMetadatasTest {	
	@Test
	public void reportTest() {
		ReportMetadatas metadata = new ReportMetadatas(
				new ReportMetadata("test", "col1", "table", true,
						new Column("col1", "number"), 
						new Column("col2", "string")
					)
				);
		try {
			String serialized = new ObjectMapper().writeValueAsString(metadata);
			assertEquals("{"
						+ "\"reportMetadatas\":["
							+ "{"
							+ "\"name\":\"test\","
							+ "\"defaultSortColumn\":\"col1\","
							+ "\"reportType\":\"table\","
							+ "\"columns\":["
									+ "{\"name\":\"col1\",\"type\":\"number\"},"
									+ "{\"name\":\"col2\",\"type\":\"string\"}"
								+ "]"
								+ ",\"showInSelector\":true"
							+ "}"
						+ "]"
					+ "}", serialized);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}