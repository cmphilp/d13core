package com.db.grad.d13.core.reports;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

@SuppressWarnings("serial")
public class ReportImplTest {	
	/*
	 assuming mocked data looks like this:
	 	col1 | col2
		1    | Lorem
		3    | ipsum
		5    | dolor
		6    | sit
		2    | ololo
		7    | oooo
	 */
	
	@Test
	public void reportTest() {
		Report report = new ReportWithMockedData("reportName", 3);
		List<Map<String, Object>> result = report.get(new HashMap<String, String>(){{
			put("sortDirection", "ASC");
			put("sortColumn", "col1");
			put("filter_col1", "2;5.8");
			put("filter_col2", "i");
			put("page", "0");
		}});
		assertEquals(1, result.size());
		Map<String, Object> row = result.get(0);
		assertEquals(3, row.get("col1"));
		assertEquals("ipsum", row.get("col2"));
	}
	
	@Test
	public void filterDescCol1() {
		Report report = new ReportWithMockedData("descFilterReport", 3);
		List <Map<String, Object>> result = report.get(new HashMap<String,String>(){{
			put("sortDirection", "DESC");
			put("sortColumn", "col1");
			put("page", "0");
		}});
		Map<String, Object> row = result.get(0); 
		assertEquals(7, row.get("col1"));
	}
	
	@Test
	public void filterAscCol1() {
		Report report = new ReportWithMockedData("ascFilterReport", 3);
		List <Map<String, Object>> result = report.get(new HashMap<String,String>(){{
			put("sortDirection", "ASC");
			put("sortColumn", "col1");
			put("page", "0");
		}});
		Map<String, Object> row = result.get(0); 
		assertEquals(1, row.get("col1"));
	}
	
	@Test
	public void filterRangeCol1() {
		Report report = new ReportWithMockedData("filterRangeReport", 3);
		List <Map<String, Object>> result = report.get(new HashMap<String, String>(){{
			put("sortDirection", "ASC");
			put("sortColumn", "col1");
			put("filter_col1", "3.4;5.8");
			put("page", "0");
		}});
		Map<String, Object> row = result.get(0); 
		assertEquals(5, row.get("col1"));
	}
	
	@Test
	public void filterEmptyCol2() {
		Report report = new ReportWithMockedData("filterStringReport", 3);
		List <Map<String, Object>> result = report.get(new HashMap<String, String>(){{
			put("sortDirection", "ASC");
			put("sortColumn", "col1");
			put("filter_col2", "a;i");
			put("page", "0");
		}}); 
		assertEquals(true, result.isEmpty());
	}
	
	@Test
	public void filterLetterCol2() {
		Report report = new ReportWithMockedData("filterStringReport2", 3);
		List <Map<String, Object>> result = report.get(new HashMap<String, String>(){{
			put("sortDirection", "ASC");
			put("sortColumn", "col1");
			put("filter_col2", "o");
			put("page", "0");
		}}); 
		Map<String, Object> row = result.get(0); 
		assertEquals("Lorem", row.get("col2"));
	}
	
	@Test
	public void filterComplCol2() {
		Report report = new ReportWithMockedData("filterStringReport2", 3);
		List <Map<String, Object>> result = report.get(new HashMap<String, String>(){{
			put("sortDirection", "DESC");
			put("sortColumn", "col2");
			put("filter_col2", "or");
			put("page", "0");
		}}); 
		Map<String, Object> row = result.get(0); 
		assertEquals("dolor", row.get("col2"));
	}
	
	@Test
	public void filterComplCol2New() {
		Report report = new ReportWithMockedData("filterStringReport2", 3);
		List <Map<String, Object>> result = report.get(new HashMap<String, String>(){{
			put("sortDirection", "DESC");
			put("sortColumn", "col2");
			put("filter_col2", "o");
			put("page", "1");
		}}); 
		Map<String, Object> row = result.get(0); 
		assertEquals("Lorem", row.get("col2"));
	}
}